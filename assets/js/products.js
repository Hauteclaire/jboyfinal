products = [
    {
        "bowl":"Shoyu ramen",
        "price": 7.99,
        "image":"/img/shoyu.jpg"
    },
    {
        "bowl":"Tomato ramen",
        "price": 9.99,
        "image":"/img/tomato.jpg"
    },
    {
        "bowl":"Jonny's Special",
        "price": 12.99,
        "image":"/img/jonny.png"
    },
    {
        "bowl":"Miso ramen",
        "price": 8.99,
        "image":"/img/miso.png"
    },
    {
        "bowl":"Garlic ramen",
        "price": 8.99,
        "image":"/img/garlic.png"
    },
    {
        "bowl":"Tonkatsu ramen",
        "price": 8.99,
        "image":"/img/tonkatsu.png"
    }
];
if(typeof module != 'undefined') {
    module.exports = products;
}