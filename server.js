// Import the necessary modules
const express = require('express');
const app = express();
const port = 1337;

// Load third-party frameworks
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/css', express.static(__dirname + '/node_modules/shards-ui/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/shards-ui/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/assets/css'));
app.use('/js', express.static(__dirname + '/assets/js'));
app.use('/img', express.static(__dirname + '/assets/img'));

// Route to views
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/views/index.html');
});
app.get('/order', function (req, res) {
    res.sendFile(__dirname + '/views/order.html');
});
app.get('/about', function (req, res) {
    res.sendFile(__dirname + '/views/about.html');
});
app.get('/login', function (req, res) {
    res.sendFile(__dirname + '/views/user.html');
});
app.post('/authenticate', function (req, res) {
    // Fill this shit out
});
app.get('/404', function (req, res) {
    res.sendFile(__dirname + '/views/404.html');
});

// Launch the server
app.listen(port, () => console.log("Jonny's Ramen listening on :1337."));